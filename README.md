
# Setup

1. Install [Node.js](https://nodejs.org/) (10.0.0 or later)
2. Clone this repository
3. Move to the `reveal.js` folder and install dependencies:
```
$ cd reveal.js && npm install
```
4. Serve the presentation and monitor source files for changes:
```
$ npm start
```
5. Open http://localhost:8000 to view your presentation